
# 🐧 Modifications personnalisées du Code Source Linux 6.0.7 🛠️

Bienvenue dans ce dépôt Git qui héberge les modifications apportées au code source du noyau Linux version 6.0.7. Il est important de noter que nous avons choisi cette version spécifique pour des raisons de simplicité et d'accessibilité, même si elle n'est pas la dernière version disponible. Ces modifications peuvent toutefois être adaptées à d'autres versions, y compris les plus récentes.

## 🌟 Introduction

Ce dépôt contient des modifications apportées au code source du noyau Linux 6.0.7. Bien que cela puisse sembler un peu en retrait par rapport à la dernière version, cela facilite l'accès et la compréhension des changements apportés. Les améliorations introduites ici pourraient également être appliquées à des versions plus récentes du noyau Linux avec quelques ajustements.

## 📂 Nouvelle Fonction d'Entrée

Une nouvelle fonction servant de point d'entrée a été ajoutée au fichier `ipv4_tcp.c`, basée sur la fonction `tcp_v4_rcv`. Cette fonction personnalisée illustre comment nous pouvons intégrer des traitements spécifiques au niveau du noyau lors de la réception de paquets TCP.

## 🧰 Helpers Personalisés

Les nouveaux helpers ont été implémentés dans le fichier `kernel/bpf/bpf_stack_tcp.c` et `.h`, ainsi que dans les fichiers de déclarations tels que `include/linux/bpf.h` et les fichiers sources tels que `kernel/bpf/helpers.c`. Ces helpers offrent des fonctionnalités supplémentaires pour améliorer la flexibilité et l'efficacité des traitements eBPF et XDP.

Néanmoins, par manque de temps, le verifier du BPF ne vérifie pas tout ce qu'il devrait faire en production. De plus, les documentations internes du noyau Linux n'ont pas été mis à jour, il ne faut donc pas les regénérer.

## 🌀 Exposition de Fonctions Privées

Dans le but de faciliter l'utilisation des fonctionnalités ajoutées, certaines fonctions privées ont été exposées. Par exemple, des fonctions pour manipuler un ringbuffer en XDP ont été ajoutées, avec diverses fonctions supplémentaires incluses dans son en-tête (`kernel/bpf/ringbuff.c` et `.h`). Cela vise à simplifier les opérations courantes et à faciliter la personnalisation du comportement du noyau.

## 🛠️ Prise en Main

Cette version modifiée du Kernel Linux se compile comme les autres. Vous devez donc : 
- Cloner ce dépôt
- Vous rendre dans ce dossier
- Effectuer la commande `make menuconfig`
- Effectuer la commande `make`
- Effectuer la commande `make headers_install`puis `make modules_install` puis `make install`
- Mettre à jour votre bootloader si make install ne l'a pas fait
- Reboot votre machine