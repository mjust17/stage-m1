#ifndef BPF_STACK_TCP_H
#define BPF_STACK_TCP_H

#include <linux/bpf.h>
#include <linux/btf.h>
#include <linux/bpf-cgroup.h>
#include <linux/rcupdate.h>
#include <linux/random.h>
#include <linux/smp.h>
#include <linux/topology.h>
#include <linux/ktime.h>
#include <linux/sched.h>
#include <linux/uidgid.h>
#include <linux/filter.h>
#include <linux/ctype.h>
#include <linux/jiffies.h>
#include <linux/pid_namespace.h>
#include <linux/proc_ns.h>
#include <linux/security.h>
#include <linux/btf_ids.h>
#include <net/xdp.h>
#include <linux/skbuff.h>
#include <linux/printk.h>
#include <net/tcp.h>
#include <net/dst.h>


/*
	Actuellement, nous n'avons que 3 flags.
*/
enum Flags_TCP_stack {
	WITH_CHECKSUM = 1, // On désactive le calcul de checksum
	WITH_TCPFILTER = 2, // On désactive le tcpfilter
	WITH_XFRM = 4 // On désactive les xfrm
};


#endif