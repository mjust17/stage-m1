#include <linux/bpf.h>
#include <linux/btf.h>
#include <linux/bpf-cgroup.h>
#include <linux/rcupdate.h>
#include <linux/random.h>
#include <linux/smp.h>
#include <linux/topology.h>
#include <linux/ktime.h>
#include <linux/sched.h>
#include <linux/uidgid.h>
#include <linux/filter.h>
#include <linux/ctype.h>
#include <linux/jiffies.h>
#include <linux/pid_namespace.h>
#include <linux/proc_ns.h>
#include <linux/security.h>
#include <linux/btf_ids.h>
#include <net/xdp.h>
#include <linux/skbuff.h>
#include <linux/printk.h>
#include <net/tcp.h>
#include <net/dst.h>
#include <linux/netdevice.h>
#include "ringbuff.h"
#include <uapi/linux/bpf.h>

#define NB_MAX_CONNECTION 64

/*
	Chaque interface réseau à sa propre table de routage. Afin d'avoir accès à la structure dst, nous avons besoin de la garder.
*/
struct rtable * table;



/*
	Un TCP-CXDP est constitué d'un buffer préalloué, d'un pointeur vers la socket ainsi que d'une liste de flags.
*/
struct tcp_stack {
	struct sk_buff skb;
	struct sock * sk;
	bool used;
	enum Flags_TCP_stack flags;
};

/*
	On a 4 état que le helper peut retourner
*/
enum State_TCP_stack {
	CONNECTION_CLOSED = 1, // La connexion vient de se terminer
	CONNECTION_NOT_FOUND = 2, // La connexion n'a pas été trouvé
	CONNECTION_PASS = 4, // On demande à ce que le paquet soit renvoyé dans le chemin classique
	CONNECTION_DROP = 8 // On indique qu'on peut drop le paquet, il a bien été traité
};

/*
	On a notre tableau avec tous nos TCP-CXDP préalloués
*/
static struct tcp_stack tab_tcp_stack[NB_MAX_CONNECTION];

/*
	Met à jour le buffer préalloué en fonction du paquet
*/
struct sk_buff *update_skb_with_buff(struct sk_buff *skb, struct xdp_buff *buff)
{
	unsigned int headroom, frame_size;
	void *hard_start;

	struct net_device *dev = buff->rxq->dev;
	struct xdp_frame *xdpf = xdp_convert_buff_to_frame(buff);

	memset(skb, 0, offsetof(struct sk_buff, queue_mapping));
	//memset(skb, 0, offsetof(struct sk_buff, tail));

	headroom = sizeof(*xdpf) + xdpf->headroom;

	frame_size = xdpf->frame_sz;

	hard_start = xdpf->data - headroom;
	skb = build_skb_around(skb, hard_start, frame_size);

	skb_reserve(skb, headroom);
	__skb_put(skb, xdpf->len);

	skb->protocol = eth_type_trans(skb, dev);
	skb->skb_iif = dev->ifindex;
	skb->data += 20;
	skb->len -= 20;
	skb->mac_len = 0xe;
	skb->network_header = skb->mac_header + skb->mac_len;
	skb->transport_header = skb->network_header + 20;
	skb->ip_summed = CHECKSUM_UNNECESSARY;
	skb->napi_id = buff->rxq->napi_id;
	skb->slow_gro = 0;
	skb->_skb_refdst = (unsigned long)&table->dst;
	skb->cb[0] = dev->ifindex;
	return skb;
}

/*
	Cette fonction prend un skb, un offset et une taille afin de copier le payload du buffer à partir de l'offset
	pour une longueur de size octet. Les données seront copiés dans le ringbuffer donné comme argument à rd.
*/
// int sk_read_bpf(read_descriptor_t *rd, struct sk_buff *skb, unsigned int offset, size_t size)
// {
// 	struct bpf_ringbuf_map *rb_map;
// 	struct bpf_map *map = rd->arg.data;

// 	// On récupère le ringbuffer
// 	rb_map = container_of(map, struct bpf_ringbuf_map, map);
// 	// On réserver de la place dedans pour y coller notre paquet
// 	void *rec = __bpf_ringbuf_reserve(rb_map->rb, size - offset);
	
// 	if(!rec)
// 		return 0;

// 	memcpy(rec, skb->data, size - offset);
// 	// On informe le userspace de notre changement
// 	bpf_ringbuf_commit(rec, 0, false);
	
// 	// On retourne le fait que l'on a lu tout le sk_buff
// 	return size - offset;
// }

/*
Premier helper initialement créé. 
Celui-ci prend le paquet, l'index du TCP-CXDP ainsi que le ringbuffer vers lequel copié les données.
Pour les détails, merci de vous référer au nouvel helper ci-dessous

ATTENTION ! IL NE SUFFIT PAS DE DECOMMENTER CETTE FONCTION POUR POUVOIR L'APPELER,
 IL FAUT AUSSI CHANGER LA STRUCTURE bpf_xdp_transmit_tcp_stack_proto EN BAS DE CE FICHIER
*/

// BPF_CALL_2(bpf_xdp_ringbuffer_tcp_stack, struct xdp_buff *, buff, int, tcp_fd, struct bpf_map *, map)
// {
// 	struct tcp_stack * tcp_data;
// 	struct sk_buff *clone;
// 	bool refcounted;
// 	void *rec;
// 	__u8 ret;
// 	if(tcp_fd < 0 || tcp_fd >= NB_MAX_CONNECTION)
// 		goto not_found;
// 	tcp_data = tab_tcp_stack + tcp_fd;
// 	if (tcp_data->used == false)
// 		goto not_found;

// 	struct bpf_ringbuf_map *rb_map = container_of(map, struct bpf_ringbuf_map, map);

// 	read_descriptor_t rd_desc = {
// 			.arg.data = rb_map,
// 			.count = 1,
// 		};

// 	void * data = buff->data + 20 + 0xe;
// 	const struct tcphdr *th = data;
	
// 	if(tcp_data->sk) {
// 		while(tcp_data->sk->sk_receive_queue.qlen > 0 && tcp_read_sock(tcp_data->sk, &rd_desc, sk_read_bpf2));
// 		if(tcp_data->sk->sk_state != TCP_ESTABLISHED) 
// 			tcp_data->sk = NULL;
// 		else if(tcp_data->sk->sk_receive_queue.qlen > 0)
// 			goto test;
// 	}
	
	
// 	if(tcp_data->sk && tcp_sk(tcp_data->sk)->rcv_nxt != ntohl(th->seq)) {
// 		goto out_of_order;
// 	}



// 	if(rb_map->rb->mask + 1 - ringbuf_avail_data_sz(rb_map->rb) < buff->data_end - data) {
// 		goto test;
// 	}

// 	update_skb_with_buff(&tcp_data->skb, buff);


// 	if (tcp_data->sk == NULL)
// 	{
// 		struct net *net = dev_net(tcp_data->skb.dev);
// 		int sdif = inet_sdif(&tcp_data->skb);
// 		const struct iphdr *iph;
// 		th = (const struct tcphdr *)tcp_data->skb.data;

// 		iph = (const struct iphdr *)ip_hdr(&tcp_data->skb);
// 		tcp_data->sk = __inet_lookup_skb(&tcp_hashinfo, &tcp_data->skb, __tcp_hdrlen(th),
// 							   th->source, th->dest, sdif, &refcounted);
// 	} else {
// 		sock_hold(tcp_data->sk);
// 		refcounted = true;
// 	}

// 	clone = skb_clone(&tcp_data->skb, GFP_ATOMIC);

// 	ret = bpf_tcp_v4_rcv(clone, tcp_data->sk, refcounted);

// 	if (tcp_data->sk)
// 	{
// 		while(tcp_data->sk->sk_receive_queue.qlen > 0 && tcp_read_sock(tcp_data->sk, &rd_desc, sk_read_bpf2));
// 	}
// 	if(th->fin || th->rst ) {
// 		return CONNECTION_CLOSED | CONNECTION_DROP;
// 	} else {
// 		return CONNECTION_DROP;

// 	}
	
	
// not_found:
// 	ret = CONNECTION_NOT_FOUND;
// 	goto test;

// out_of_order:	
// 	//On fait rien pour l'instant

// test:
// 	if(th->fin || th->rst ) {
// 		return ret | CONNECTION_DROP;
// 	}
	
// 	return ret | CONNECTION_PASS;

// 	return 0;
// }


/*
	Cette fonction ressemble à la fonction sk_read_bpf mais au lieu de copier les données, elle appelle le callback.
*/
int sk_read_bpf2(read_descriptor_t *rd, struct sk_buff *skb, unsigned int offset, size_t size)
{
	bpf_callback_t callback = (bpf_callback_t)rd->arg.data;
	
	u64 ret = callback((u64)skb->data, size, offset, 0, 0);

	return size - offset;
}

/*
	Ce helper prend le paquet, l'index du TCP-CXDP ainsi que le callback à appeler.
	Pour les détails, merci de vous référer au nouvel helper ci-dessous
*/
BPF_CALL_3(bpf_xdp_transmit_tcp_stack, struct xdp_buff *, buff, int, tcp_fd, void *, callback)
{
	struct tcp_stack * tcp_data;
	struct sk_buff *clone;
	bool refcounted;
	void *rec;
	__u8 ret;
	//On retrouve la connexion associée
	if(tcp_fd < 0 || tcp_fd >= NB_MAX_CONNECTION)
		goto not_found;
	tcp_data = tab_tcp_stack + tcp_fd;
	if (tcp_data->used == false)
		goto not_found;

	//On prépare les structures nécessaires
	read_descriptor_t rd_desc = {
			.arg.data = callback,
			.count = 1,
		};

	void * data = buff->data + 20 + 0xe;
	const struct tcphdr *th = data;

	//Si on a déjà un pointeur vers une socket	
	if(tcp_data->sk) {
		while(tcp_data->sk->sk_receive_queue.qlen > 0 && tcp_read_sock(tcp_data->sk, &rd_desc, sk_read_bpf2));///On lit tout ce qu'on peut
		if(tcp_data->sk->sk_state != TCP_ESTABLISHED) //Si l'état a changé, alors il y a de forte chance que la socket change aussi
			tcp_data->sk = NULL;
		else if(tcp_data->sk->sk_receive_queue.qlen > 0)//Si on a pas pu tout lire, alors on va demander à renvoyer le paquet dans le chemin classique
			goto test;
	}

	

	// Si le paquet n'est pas celui qu'on attend (out of order)
	if(tcp_data->sk && tcp_sk(tcp_data->sk)->rcv_nxt != ntohl(th->seq)) {
		goto out_of_order;
	}

	//On le met à jour
	update_skb_with_buff(&tcp_data->skb, buff);

	// Si on a pas encore de socket préenregistré
	if (tcp_data->sk == NULL)
	{
		//On va la rechercher
		struct net *net = dev_net(tcp_data->skb.dev);
		int sdif = inet_sdif(&tcp_data->skb);
		const struct iphdr *iph;
		th = (const struct tcphdr *)tcp_data->skb.data;

		iph = (const struct iphdr *)ip_hdr(&tcp_data->skb);
		tcp_data->sk = __inet_lookup_skb(&tcp_hashinfo, &tcp_data->skb, __tcp_hdrlen(th),
							   th->source, th->dest, sdif, &refcounted);
	} else {// sinon
		//on en prend une référence
		sock_hold(tcp_data->sk);
		refcounted = true;
	}

	// On clone le buffer. ATTENTION ! ON DEVRAIT SUPPRIMER CETTE LIGNE POUR GAGNER EN PERFORMANCE
	clone = skb_clone(&tcp_data->skb, GFP_ATOMIC);

	// On appelle notre nouveau point d'entrée
	ret = bpf_tcp_v4_rcv(clone, tcp_data->sk, refcounted, tcp_data->flags);

	if (tcp_data->sk)
	{
		//On va lire le maximum de données possibles
		while(tcp_data->sk->sk_receive_queue.qlen > 0 && tcp_read_sock(tcp_data->sk, &rd_desc, sk_read_bpf2));
	}

	//Si la connexion est terminé
	if(th->fin || th->rst ) {
		return CONNECTION_CLOSED | CONNECTION_DROP;
	} else {//Sinon
		return CONNECTION_DROP;

	}

// On a pas trouvé le TCP-CXDP
not_found:
	ret = CONNECTION_NOT_FOUND;
	goto test;

out_of_order:	
	//On fait rien pour l'instant

// On a pas pu lire assez de données
test:
	if(th->fin || th->rst ) {
		return ret | CONNECTION_DROP;
	}
	
	return ret | CONNECTION_PASS;
}

/*
	Helper permettant de créer un nouveau TCP-CXDP
*/
BPF_CALL_2(bpf_xdp_init_tcp_stack, struct xdp_buff *, buff, enum Flags_TCP_stack, flags) {
	if(table == NULL) {//On initialise notre tableau de TCP-CXDP
		for(int i = 0; i<NB_MAX_CONNECTION; ++i)
			tab_tcp_stack[i].used = false;
		table = ip_route_output(dev_net(buff->rxq->dev), 0, htonl(INADDR_LOOPBACK), 0, 0);
	}

	for(int i = 0; i<NB_MAX_CONNECTION; ++i) {//On cherche un TCP-CXDP libre
		if(!tab_tcp_stack[i].used) {
			tab_tcp_stack[i].sk = NULL;
			tab_tcp_stack[i].used = true;
			tab_tcp_stack[i].flags = flags;
			return i;
		}
	}
	return -1;
}

/*
	Helper permettant de libérer notre TCP-CXDP
*/
BPF_CALL_1(bpf_xdp_destroy_tcp_stack, int, tcp_fd) {
	if(tcp_fd >= 0 && tcp_fd < NB_MAX_CONNECTION) {
		tab_tcp_stack[tcp_fd].used = false;//On libère le TCP-CXDP
	}
	return 0;
}


const struct bpf_func_proto bpf_xdp_destroy_tcp_stack_proto = {
	.func		= bpf_xdp_destroy_tcp_stack,
	.gpl_only	= false,
	.ret_type	= RET_VOID,
	.arg1_type = ARG_ANYTHING,
};

const struct bpf_func_proto bpf_xdp_transmit_tcp_stack_proto = {
	.func		= bpf_xdp_transmit_tcp_stack,
	.gpl_only	= false,
	.ret_type	= RET_INTEGER,
	.arg1_type = ARG_ANYTHING,
	.arg2_type = ARG_ANYTHING,
	.arg3_type	= ARG_PTR_TO_FUNC,
};


const struct bpf_func_proto bpf_xdp_init_tcp_stack_proto = {
	.func = bpf_xdp_init_tcp_stack,
	.gpl_only = false,
	.ret_type = RET_INTEGER,
	.arg1_type = ARG_ANYTHING,
	.arg2_type = ARG_ANYTHING
};
