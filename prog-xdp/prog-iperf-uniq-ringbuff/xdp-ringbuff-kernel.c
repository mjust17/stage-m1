#include <linux/bpf.h>
#include <bpf/bpf_helpers.h>
#include <linux/in.h>
#include <linux/ip.h>
#include <linux/if_ether.h>
#include <bpf/bpf_endian.h>
#include <linux/tcp.h>
#include <linux/in.h>
#include <string.h>

struct {
	__uint(type, BPF_MAP_TYPE_RINGBUF);
	__uint(max_entries, 65536 * 1000 /* max size possible */);
} rb SEC(".maps");


enum State_TCP_stack {
	CONNECTION_CLOSED = 1,
	CONNECTION_NOT_FOUND = 2,
	CONNECTION_PASS = 4,
	CONNECTION_DROP = 8
};


static int tcp_stack = -1;



SEC("test")
int isTCP(struct xdp_md *ctx)
{
    void *data_begin = (void *)(long)ctx->data;
    void *data_end = (void *)(long)ctx->data_end;
    struct ethhdr *eth = data_begin;
    if ((void *)(eth + 1) > data_end)
    {
        return XDP_PASS;
    }

    if (eth->h_proto == bpf_htons(ETH_P_IP))
    {
        struct iphdr *ip = (void *)(eth + 1);
        if((void *)(ip + 1) > data_end)
            return XDP_PASS;

        if (ip->protocol == IPPROTO_TCP) {
            struct tcphdr * tcp = (void *)(ip + 1);
            if((void *)(tcp + 1) > data_end)
                return XDP_PASS;
            if(bpf_htons(tcp->dest) == 8080) {
                if(tcp_stack == -1) {
                    tcp_stack = bpf_xdp_init_tcp_stack(ctx);
                }
                if(tcp_stack != -1) {
                    __u32 ret = bpf_xdp_ringbuffer_tcp_stack(ctx, tcp_stack, &rb);
                    if(ret & CONNECTION_NOT_FOUND)
                        tcp_stack = -1;
                    if(ret & CONNECTION_PASS)
                        return XDP_PASS;
                    if(ret & CONNECTION_CLOSED) {
                        bpf_xdp_destroy_tcp_stack(tcp_stack);
                        tcp_stack = -1;
                    }
                    return XDP_DROP;
                }
                return XDP_PASS;
            }
        }
    }
    return XDP_PASS;
}

char LICENSE[] SEC("license") = "GPL";
