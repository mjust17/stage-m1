# 🚀 Dépôt Git de Programmes XDP 🌐

Bienvenue dans le dépôt Git contenant une collection de programmes XDP (eXpress Data Path) que j'ai créés pour explorer et comprendre la puissance de la programmation eBPF dans le contexte du traitement des paquets réseau.

*Ce dépôt est inspiré par le fameux* ***xdp-tutorial*** *qui fournit d'excellents exemples et guides sur l'utilisation de XDP et eBPF pour des applications réseau avancées.*

## 📜 Introduction

Ce dépôt regroupe divers exemples et expérimentations mettant en œuvre XDP et eBPF pour manipuler et optimiser les performances des paquets réseau. Les programmes XDP permettent une personnalisation profonde du traitement des paquets directement au niveau du noyau.

## 📂 Programme de Prise en Main

### **xdp-ringbuffer** 📦

Le dossier 📁 `xdp-ringbuffer` contient une implémentation XDP qui extrait le header IP de chaque paquet IP reçu et le transmet à l'espace utilisateur. Ceci sert de point de départ pour comprendre comment XDP peut être utilisé pour une transmission efficace des informations du header IP.

## 🚀 Expérimentations

Explorez ces expérimentations pour découvrir comment XDP peut être utilisé pour améliorer les performances réseau.

### **prog-iperf-uniq-normal** 🌐

Le dossier 📁 `prog-iperf-uniq-normal` abrite un programme serveur utilisant iPerf. L'objectif est d'obtenir le maximum de débit possible. Ce projet agit comme référence de performance pour les expérimentations suivantes.

### **prog-iperf-uniq-ringbuff** ⚙️

Le dossier 📁 `prog-iperf-uniq-ringbuff` présente une implémentation XDP pour améliorer les performances de `prog-iperf-uniq-normal`. Le premier helper XDP est utilisé pour accélérer le traitement. 

⚠️ **Note :** En raison des changements dans le code source de Linux, ce projet ne peut plus être exécuté directement. Il faudra remodifier le code source du noyau Linux comme expliqué afin de le relancer.

### **prog-iperf-uniq-callback** 🔄

Le dossier 📁 `prog-iperf-uniq-callback` propose également une implémentation XDP similaire à `prog-iperf-uniq-normal`. Cependant, cette fois, le deuxième helper XDP est utilisé. Vous pouvez exécuter ce projet sous certaines conditions.

✅ **Validation :** Ce projet peut être lancé si toutes les conditions sont remplies.

## 🛠️ Prise en Main

Afin de pouvoir lancer les différents programmes, vous devez : 
- Avoir installé le Kernel modifié 
- Avoir copié le dépôt
- Effectuer la commande make à la racine
- Vous rendre dans le dossier de votre choix et lancer le programme avec la commande :  `./xdp-ringbuff-user <nom de l'interface> attach <nombre de messages attendus> <taille des messages attendus>` par exemple `./xdp-ringbuff-user en0 attach 100000 64`. 
- Vous devrez ensuite envoyé depuis un programme externe le même nombre de message de la même taille en utilisant le protocole TCP. Vous trouverez dans le dossier `client` un exemple de programme qu'il faut compiler avec gcc. 

## 💪📊 Tests de performances

Afin de réaliser les tests de performances, vous pouvez utiliser le script npf [disponible ici](https://forge.uclouvain.be/ensg/xdp-tcp-fastpath-expe). Vous devrez pour cela avoir installer [Network Performance Measurement](https://github.com/tbarbette/npf).
