#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <linux/if_link.h>
#include <signal.h>
#include <net/if.h>
#include <assert.h>
#include <linux/tcp.h>


/* In this example we use libbpf-devel and libxdp-devel */
#include <bpf/bpf.h>
#include <bpf/libbpf.h>
#include <arpa/inet.h>
#include "../common/common_params.h"
#include "../common/common_user_bpf_xdp.h"

int process(void * ctx, void * data, size_t data_sz) {
    struct tcphdr * tcp = data; 
    printf("tcp->source = %i, tcp->dest = %i\n", htons(tcp->source), htons(tcp->dest));
    return 0;
}

static volatile bool exiting = false;

static void sig_handler(int sig)
{
	exiting = true;
}

int main(int argc, char *argv[])
{
    struct bpf_object *bpf_obj;
    int err;

    signal(SIGINT, sig_handler);
	signal(SIGTERM, sig_handler);

    if (argc != 3)
    {
        printf("Usage: %s <IFNAME> <detach|attach>\n", argv[0]);
        return 1;
    }

    struct config cfg = {
        .xdp_flags = XDP_FLAGS_DRV_MODE,
        .filename = "xdp-ringbuff-kernel.o",
        .progname = "isTCP"
    };
    


    cfg.ifindex = if_nametoindex(argv[1]);
    if (cfg.ifindex == -1)
    {
        printf("Erreur : le device n'existe pas.");
        return 1;
    }

    if (strcmp(argv[2], "detach") == 0)
    {
        cfg.unload_all = true;
        do_unload(&cfg);
        return 0;
    }

    bpf_obj = xdp_program__bpf_obj(load_bpf_and_xdp_attach(&cfg));
    int buffer_map_fd = bpf_object__find_map_fd_by_name(bpf_obj, "rb");
    printf("fd = %i\n", buffer_map_fd);
    struct ring_buffer * rb = ring_buffer__new(buffer_map_fd, process, NULL, NULL);

    printf("Programme chargé !\n");
    while (!exiting)
    {   
        err = ring_buffer__poll(rb, 100 /* timeout, ms */);
		/* Ctrl-C will cause -EINTR */
		if (err == -EINTR) {
			err = 0;
			break;
		}
		if (err < 0) {
			printf("Error polling ring buffer: %d\n", err);
			break;
		}
    }

    return 0;
}