#include <linux/bpf.h>
#include <bpf/bpf_helpers.h>
#include <linux/in.h>
#include <linux/ip.h>
#include <linux/if_ether.h>
#include <bpf/bpf_endian.h>
#include <linux/tcp.h>
#include <linux/in.h>
#include <string.h>


struct {
	__uint(type, BPF_MAP_TYPE_RINGBUF);
	__uint(max_entries, 256 * 1024 /* 256 KB */);
} rb SEC(".maps");


SEC("test")
int isTCP(struct xdp_md *ctx)
{
    void *data_begin = (void *)(long)ctx->data;
    void *data_end = (void *)(long)ctx->data_end;
    struct ethhdr *eth = data_begin;
    if ((void *)(eth + 1) > data_end)
    {
        return XDP_PASS;
    }

    if (eth->h_proto == bpf_htons(ETH_P_IP))
    {
        struct iphdr *ip = (void *)(eth + 1);
        if((void *)(ip + 1) > data_end)
            return XDP_PASS;

        if (ip->protocol == IPPROTO_TCP) {
            struct tcphdr * tcp = (void *)(ip + 1);
            if((void *)(tcp + 1) > data_end)
                return XDP_PASS;
            
            struct tcphdr * tmp = bpf_ringbuf_reserve(&rb, sizeof(*tmp), 0);
            if(!tmp)
                return 0;

            memcpy(tmp, tcp, sizeof(*tcp));

            bpf_ringbuf_submit(tmp, 0);

        }
    }
    return XDP_PASS;
}

char LICENSE[] SEC("license") = "GPL";
