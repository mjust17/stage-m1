#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <linux/if_link.h>
#include <signal.h>
#include <net/if.h>
#include <assert.h>
#include <linux/tcp.h>
#include <bpf/bpf.h>
#include <bpf/libbpf.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include "../common/common_params.h"
#include "../common/common_user_bpf_xdp.h"
#include <time.h>

#define IP INADDR_ANY
#define PORT 8080

const int DEBUG532 = 1;

void setup_socket(int socket_descriptor)
{
    int return_code;

    struct sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    return_code = bind(
        socket_descriptor,
        (struct sockaddr *)&address,
        sizeof(address));

    if (return_code == -1)
    {
        perror("Error binding socket to address");
        exit(-1);
    }
    return_code = listen(socket_descriptor, 10);

    if (return_code == -1)
    {
        perror("Could not start listening on socket");
    }
}

int create_socket()
{
    // File descriptor for the socket
    int socket_descriptor;

    socket_descriptor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (socket_descriptor == -1)
    {
        perror("Error opening socket on server-side");
        exit(-1);
    }
    int flag = 1;
    if (-1 == setsockopt(socket_descriptor, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag)))
    {
        perror("setsockopt fail");
    }
    setup_socket(socket_descriptor);

    return socket_descriptor;
}

int accept_connection(int socket_descriptor)
{

    int connection = accept(
        socket_descriptor,
        NULL, NULL);

    if (connection == -1)
    {
        perror("Error accepting connection");
    }
    if (DEBUG532)
        printf("Connection accepté\n");

    return connection;
}

#define MIN(A, B) (A < B ? A : B)

void readAll(int sock, void *buffer, long size)
{
    long readAct = 0, ret;
    while (readAct < size)
    {
        ret = read(sock, buffer + readAct, MIN(size, size - readAct));
        if (ret < 0)
        {
            perror("Erreur d'écriture\n");
            exit(-1);
        }
        readAct += ret;
    }
}

void writeAll(int sock, void *buffer, long size)
{
    long writeAct = 0, ret;
    while (writeAct < size)
    {
        ret = write(sock, buffer + writeAct, size - writeAct);
        if (ret < 0)
        {
            perror("Erreur d'écriture\n");
            exit(-1);
        }
        writeAct += ret;
    }
}

int main(int argc, char *argv[])
{
    struct bpf_object *bpf_obj;
    int err;

    if (argc != 5)
    {
        printf("Usage: %s <IFNAME> <detach|attach> <nb> <size>\n", argv[0]);
        return 1;
    }

    long nb = atol(argv[3]);
    long size = atol(argv[4]);
    long dataExpected = nb * size;
    struct tcp_info tcp_info;
    if (DEBUG532)
        printf("J'attends %i paquets de %i o chacun, soit %i octet\n", nb, size, dataExpected);

    struct config cfg = {
        .xdp_flags = XDP_FLAGS_DRV_MODE,
        .filename = "xdp-ringbuff-kernel.o",
        .progname = "isTCP"};

    cfg.ifindex = if_nametoindex(argv[1]);
    if (cfg.ifindex == -1)
    {
        printf("Erreur : le device n'existe pas.");
        return 1;
    }

    if (strcmp(argv[2], "detach") == 0)
    {
        cfg.unload_all = true;
        do_unload(&cfg);
        return 0;
    }

    bpf_obj = xdp_program__bpf_obj(load_bpf_and_xdp_attach(&cfg));
    if (DEBUG532)
        printf("Programme chargé !\n");

    int socket_descriptor;
    int connection;

    socket_descriptor = create_socket();
    connection = accept_connection(socket_descriptor);
    if(DEBUG532)    
        printf("La connection est réussi\n");
    struct timespec tstart = {0, 0}, tend = {0, 0};
    clock_gettime(CLOCK_MONOTONIC, &tstart);

    char buffer[size];

    __u32 crc = 0;
    for (int i = 0; i < nb; ++i)
    {
        readAll(connection, buffer, size);
        for (int i = 0; i < size / 4; ++i)
            crc |= *(__u32 *)(buffer + size * 4);
    }

    clock_gettime(CLOCK_MONOTONIC, &tend);

    char c;
    writeAll(connection, &c, 1);

    double sum = (((double)tend.tv_sec * 1.0e3 + 1.0e-6 * tend.tv_nsec) -
                  ((double)tstart.tv_sec * 1.0e3 + 1.0e-6 * tstart.tv_nsec));
    double nbBitsSent = (double)dataExpected / sum * 1000 * 8;

    if (1)
        printf("time = %f, throughput = %.2fGb/s\n", sum, nbBitsSent / 1e9);
    else
        printf("%.2f\n", nbBitsSent / 1e9);

    if (DEBUG532)
        printf("Youpi ça marche\n");

    cfg.unload_all = true;
    do_unload(&cfg);

    close(connection);
    close(socket_descriptor);

    return 0;
}
