#include <linux/bpf.h>
#include <bpf/bpf_helpers.h>
#include <linux/in.h>
#include <linux/ip.h>
#include <linux/if_ether.h>
#include <bpf/bpf_endian.h>
#include <linux/tcp.h>
#include <linux/in.h>
#include <string.h>

struct {
	__uint(type, BPF_MAP_TYPE_RINGBUF);
	__uint(max_entries, 65536 * 1000 /* max size possible */);
} rb SEC(".maps");

// struct CRC_ctx_t {
//     size_t size;
//     __u32 crc;
// };

// static long crc(__u32 loop, void * data) {
//     struct CRC_ctx_t * ctx = data;
//     if(ctx != NULL)
//         ctx->crc |= (__u32)(loop*4);
//     return 0;
// }

// static __u64 callback(void * data, size_t size, unsigned int offset) {
//     struct CRC_ctx_t CRC_ctx;
//     CRC_ctx.size = size;
//     CRC_ctx.crc = 0;
//     crc(0, NULL);
//     bpf_loop(0, &crc, &CRC_ctx, 0);
//     bpf_ringbuf_output(&rb, &size, sizeof(size_t), BPF_RB_FORCE_WAKEUP);
//     return 0;
// }

enum State_TCP_stack {
	CONNECTION_CLOSED = 1,
	CONNECTION_NOT_FOUND = 2,
	CONNECTION_PASS = 4,
	CONNECTION_DROP = 8
};

static __u64 callback(void * data, size_t size, unsigned int offset) {
    bpf_printk("bouh \n");
    bpf_ringbuf_output(&rb, &size, sizeof(size_t), BPF_RB_FORCE_WAKEUP);
    return 0;
}

static int tcp_stack = -1;



SEC("test")
int isTCP(struct xdp_md *ctx)
{
    void *data_begin = (void *)(long)ctx->data;
    void *data_end = (void *)(long)ctx->data_end;
    struct ethhdr *eth = data_begin;
    if ((void *)(eth + 1) > data_end)
    {
        return XDP_PASS;
    }

    if (eth->h_proto == bpf_htons(ETH_P_IP))
    {
        struct iphdr *ip = (void *)(eth + 1);
        if((void *)(ip + 1) > data_end)
            return XDP_PASS;

        if (ip->protocol == IPPROTO_TCP) {
            struct tcphdr * tcp = (void *)(ip + 1);
            if((void *)(tcp + 1) > data_end)
                return XDP_PASS;
            if(bpf_htons(tcp->dest) == 8080) {
                if(tcp_stack == -1) {
                    tcp_stack = bpf_xdp_init_tcp_stack(ctx);
                    bpf_printk("pointer %d\n", tcp_stack);
                }
                if(tcp_stack != -1) {
                    __u32 ret = bpf_xdp_transmit_tcp_stack(ctx, tcp_stack, &callback);
                    if(ret & CONNECTION_NOT_FOUND)
                        tcp_stack = -1;
                    if(ret & CONNECTION_PASS)
                        return XDP_PASS;
                    if(ret & CONNECTION_CLOSED) {
                        bpf_xdp_destroy_tcp_stack(tcp_stack);
                        tcp_stack = -1;
                    }
                    return XDP_DROP;
                }
                return XDP_PASS;
            }
        }
    }
    return XDP_PASS;
}

char LICENSE[] SEC("license") = "GPL";
