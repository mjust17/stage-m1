
# Stage : Expérimentations avec eBPF et Contribution au Code Source Linux

## Résumé
🚀 Mon stage a été une passionnante aventure au cœur du système d'exploitation Linux, où j'ai exploré les méandres du noyau, réalisé des expérimentations innovantes avec eBPF et contribué au code source du noyau. J'ai également approfondi mes connaissances en matière de kernel bypass et d'eBPF.

## Expérimentations avec eBPF

🔍 Dans cette phase, j'ai plongé dans l'univers passionnant d'eBPF, en réalisant diverses expérimentations. J'ai exploré les possibilités de ce puissant framework pour l'analyse des performances, la détection d'anomalies et la surveillance du réseau. Ces expérimentations m'ont permis d'acquérir une compréhension approfondie de l'écosystème eBPF et de ses applications potentielles.
Vous trouverez dans le dossier `prog-xdp` les différents programmes implémentés ainsi qu'un readme détaillant tout cela.

## Contribution au Code Source Linux
🛠️ Lors de ce stage, j'ai travaillé sur la modification et l'exposition de certaine partie du code source Linux, ce qui a renforcé ma compréhension des mécanismes internes du système d'exploitation.
Vous trouverez dans le dossier `kernel` le code source du noyau Linux ainsi que la plupart des chemins modifiés.

## État de l'Art
📚 Mon état de l'art *(dossier  `state-of-the-art`)* a porté sur deux domaines clés : le kernel bypass et eBPF.
- Dans la première partie, j'ai exploré les différentes techniques de contournement du noyau pour optimiser les performances des applications. J'ai examiné les avantages et les inconvénients du kernel bypass dans divers scénarios.
- La seconde partie de l'état de l'art s'est concentrée sur eBPF, mettant en lumière ses fonctionnalités, son architecture et ses avantages/inconvénients dans le but de l'augmentation des performances réseaux.

## Rapport et Présentation
📑 Mon rapport documente de manière plus approfondie mes expérimentations, mes modifications du kernel Linux et mon état de l'art. 
Ma présentation (disponible après ma soutenance) est un résumé plus vivant de ce rapport et se base sur une présentation que j'ai effectuée devant des membres du laboratoire dans lequel j'évoluais.

Ces deux documents sont disponibles dans le dossier (`report`)

## Remerciements
🙌 Je tiens à exprimer ma gratitude envers mon maître de stage pour son soutien constant et ses précieux conseils tout au long de mon stage. 

Je remercie également toute l'équipe pour m'avoir accueilli et permis de profiter pleinement de ce stage de 3 mois. **C'était une super expérience** 🌟

